window.onload = (function(){

var canvas = $('#main')[0];
var ctx = canvas.getContext('2d');
var imagedata = ctx.createImageData(canvas.width, canvas.height);
loadInitialImageData(document.getElementById('initial-image').innerHTML);

var echoCanvas = $('#echo')[0];
var echoCtx = echoCanvas.getContext('2d');

function loadInitialImageData(b64){
  var d = lz4ToArray(b64);
  for(var i = 0, len = imagedata.data.length; i < len; ++i){
    imagedata.data[i] = d[i];
  }
  ctx.putImageData(imagedata, 0, 0);
}

function mergeDiffImageData(b64, blocks){
  var d = lz4ToArray(b64), w = canvas.width, h = canvas.height;
  //console.log("bytes(compressed + base64)", b64.length, "bytes(decoded)", d.length);
  var x, y, ofsx, ofsy, b = 0;
  for(var i = 0; i < blocks.length; i += 2){
    ofsx = blocks[i + 0] << 6;
    ofsy = blocks[i + 1] * w << 6;
    endx = ofsx + (16 << 2);
    endy = ofsy + (w << 6);
    for(y = ofsy; y < endy; y += w << 2){
      for(x = ofsx; x < endx; ++x){
        imagedata.data[y + x] = (imagedata.data[y + x] ^ d[b++]) & 0xff;
      }
    }
    ctx.putImageData(imagedata, 0, 0, blocks[i + 0] << 4, blocks[i + 1] << 4, 16, 16);
  }
}

function fillImageData(c){
  for(var i = 0, len = imagedata.data.length; i < len; ++i){
    imagedata.data[i] = c[i & 3];
  }
  ctx.putImageData(imagedata, 0, 0);
}

function lz4ToArray(b64){
  var d = atob(b64); len = d.length;
  var buf = new Uint8Array(new ArrayBuffer(len));
  for(var i = 0; i < len; ++i){
    buf[i] = d.charCodeAt(i);
  }
  return lz4.LZ4_uncompress(buf);
}

//http://blog.seyself.com/2008/06/post_2008061020.html
function catmullRom(p0, p1, p2, p3, t){
  var v0 = (p2 - p0) / 2, v1 = (p3 - p1) / 2;
  var t2 = t * t, t3 = t2 * t;
  return (2 * p1 - 2 * p2 + v0 + v1) * t3 + ( -3 * p1 + 3 * p2 - 2 * v0 - v1) * t2 + v0 * t + p1;
}

$.fx.step.spline = function(fx){
  var pos = fx.pos;
  fx.elem.style.left = catmullRom(fx.end.p[0][0],fx.end.p[1][0],fx.end.p[2][0],fx.end.p[3][0], pos) + 'px';
  fx.elem.style.top = catmullRom(fx.end.p[0][1],fx.end.p[1][1],fx.end.p[2][1],fx.end.p[3][1], pos) + 'px';
};

var me = parseInt(document.body.getAttribute('data-user-id'), 10);
var users = {
};

function getUser(uid){
  if (!users[uid]){
    users[uid] = {
      elem: $('<li>'+uid+'</li>').appendTo('#users').hide(),
      pos: []
    };
  }
  return users[uid];
}
eventSource = new EventSource('/stream');
eventSource.addEventListener('diff', function(e){
  if (!e.data) return;
  var data = JSON.parse(e.data);
  mergeDiffImageData(data.d, data.b);
});
eventSource.addEventListener('fill', function(e){
  if (!e.data) return;
  var data = JSON.parse(e.data);
  fillImageData(data.c);
});
eventSource.addEventListener('at', function(e){
  if (!e.data) return;
  var d = JSON.parse(e.data);
  if (d.u == me) return;
  var user = getUser(d.u);
  user.pos.push([d.x,d.y]);
  if (user.pos.length > 4) user.pos.shift();
  if (user.pos.length == 4){
    user.elem.animate({spline: {p: $.extend({}, user.pos)}}, {duration: 300, easing: 'linear'});
    user.elem.fadeIn(100);
  }
});
eventSource.addEventListener('leave', function(e){
  if (!e.data) return;
  var d = JSON.parse(e.data);
  if (d.u == me) return;
  var user = getUser(d.u);
  user.pos = [];
  user.elem.fadeOut(100);
});

eventSource.onerror = function(e){
  if (eventSource.readyState == 2){
    console.log('通信が切断されました（サーバ側の都合かも？）。自動リロードします');
    eventSource.close();
    setTimeout(function(){
      location.reload(true);
    }, 1000+Math.random()*3000);
  }
};

var prevPos = {x: 0, y: 0}, lastPos = null, posTimer;
function sentAt(){
  if (posTimer){
    clearTimeout(posTimer);
  }
  if (lastPos){
    $.post('/at', lastPos);
    prevPos = lastPos;
    lastPos = null;
  } else if (prevPos) {
    $.post('/at', prevPos);
    prevPos = null;
  } else {
    //送らない
  }
  posTimer = setTimeout(sentAt, 300);
}
sentAt();

var points = null;
function pushPoint(x, y){
  points.push(x, y);
  echoCtx.strokeStyle = 'rgb(0,0,0)';
  echoCtx.beginPath();
  var len = points.length;
  if (len > 2){
    var i = len - 4;
    echoCtx.lineTo(points[i+0], points[i+1]);
    echoCtx.lineTo(points[i+2], points[i+3]);
  }
  echoCtx.stroke();
}
function draw(){
  if (!points) return;

  $.post('/draw', {pt: points.join(',')});
  points = null;
  echoCtx.clearRect(0, 0, canvas.width, canvas.height);
}
function clear(){
  $.post('/clear');
}

$('#c').on('mousedown', function(e){
  points = [];
  pushPoint(e.offsetX, e.offsetY);
}).on('mouseup', function(e){
  draw();
}).on('mousemove', function(e){
  lastPos = {x: e.offsetX, y: e.offsetY};
  if (points){
    pushPoint(lastPos.x, lastPos.y);
  }
}).on('mouseleave', function(e){
  draw();
  lastPos = {x: -1, y: -1};
});

$('a.clear').on('click', function(){
  clear();
  return false;
});

window.testDraw = function(){
  points = [40,40, 400,400];
  draw();
};

});
