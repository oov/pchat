var notifyEmitter = new (require('events').EventEmitter)();
var Canvas = require('canvas');
var lz4 = require('lz4');
var canvas = new Canvas(500, 500);
var ctx = canvas.getContext('2d');
var lastImageData;

ctx.fillStyle = "rgb(255,255,255)";
ctx.fillRect(0, 0, canvas.width, canvas.height);

var notifyIdCounter = 0;
function generateNotifyId(){
  return ++notifyIdCounter;
}

var drawIdCounter = 0;
function generateDrawId(){
  return ++drawIdCounter;
}

exports.index = function(req, res){
  if (!req.session.userId){
    req.session.userId = generateNotifyId();
  }
  req.session.leave = true;
  res.locals.userId = req.session.userId;

  var data = ctx.getImageData(0, 0, canvas.width, canvas.height);
  var buf = new Buffer(data.data.length);
  for(var i = 0, len = data.data.length; i < len; ++i){
    buf[i] = data.data[i];
  }
  var d = lz4.encode(buf);
  res.render('index', { title: 'pchat', image: d.toString('base64') });
};

exports.indexImage = function(req, res){
  res.setHeader('Content-Type', 'image/png');
  canvas.pngStream().pipe(res);
};

exports.draw = function(req, res){
  var w = canvas.width, h = canvas.height;
  var oldData = lastImageData ? lastImageData : ctx.getImageData(0, 0, w, h).data;

  ctx.strokeStyle = 'rgb('+Math.floor(Math.random()*255)+','+Math.floor(Math.random()*255)+','+Math.floor(Math.random()*255)+')';
  ctx.beginPath();
  var pts = req.body.pt.split(',');
  for(var i = 0; i < pts.length; i += 2){
    ctx.lineTo(parseInt(pts[i], 10), parseInt(pts[i+1], 10));
  }
  ctx.stroke();

  res.send(200, '1');

  var newData = ctx.getImageData(0, 0, w, h).data;
  var x, y, c, i = 0, blocks = {}, key, blockX, blockY;
  for(y = 0; y < h; ++y){
    for(x = 0; x < w<<2; ++x, ++i){
      oldData[i] = c = (oldData[i] ^ newData[i]) & 0xff;
      if (c){
        blockX = (x>>2) & ~0xf;
        blockY = y & ~0xf;
        key = blockX + ',' + blockY;
        if (!(key in blocks)){
          blocks[key] = [blockX, blockY];
        }
      }
    }
  }

  lastImageData = newData;

  var keys = Object.keys(blocks);
  if (!keys.length) return;

  var rkeys = [];
  var i, pos, posx, posy, x, endx, y, endy, b = 0;
  var buf = new Buffer(keys.length << 10);
  for(i = 0, key; i < keys.length; ++i){
    pos = blocks[keys[i]];
    posx = pos[0];
    posy = pos[1];
    
    rkeys.push(posx >> 4);
    rkeys.push(posy >> 4);

    endx = (posx + 16) << 2;
    endy = ((posy + 16) * w) << 2;
    for(y = posy * w << 2; y < endy; y += w << 2){
      for(x = y + (posx << 2); x < endx + y; ++x, ++b){
        buf[b] = oldData[x];
      }
    }
  }
 
  var d = lz4.encode(buf);
  notifyEmitter.emit('at', {
    event: 'diff',
    data: JSON.stringify({
      b: rkeys,
      d: d.toString('base64'),
      u: req.session.userId
    })
  });

};

exports.clear = function(req, res){
  ctx.fillStyle = "rgb(255,255,255)";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  lastImageData = null;
  res.send(200, '1');
  notifyEmitter.emit('at', {
    event: 'fill',
    data: JSON.stringify({
      c: [255, 255, 255, 255],
      u: req.session.userId
    })
  });
};

exports.at = function(req, res){
  var x = parseInt(req.body.x, 10), y = parseInt(req.body.y, 10);
  if (!req.session.userId || isNaN(x) || isNaN(y)){
    return res.send(403, 'bye');
  }
  if (x != -1 && y != -1){
    notifyEmitter.emit('at', {
      event: 'at',
      data: JSON.stringify({x: x, y: y, u: req.session.userId})
    });
  } else {
    notifyEmitter.emit('at', {
      event: 'leave',
      data: JSON.stringify({u: req.session.userId})
    });
  }
  res.send(200, '');
};

exports.stream = function(req, res){
  function write(o){
    res.write('event: '+o.event+'\n');
    if (o.id){
      res.write('id: '+o.id+'\n');
    }
    res.write('data: '+o.data+'\n');
    res.write('\n');
  }
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'X-Accel-Buffering': 'no'
  });
  req.on('close', function(){
    notifyEmitter.removeListener('at', write);
  });
  notifyEmitter.on('at', write);
};
